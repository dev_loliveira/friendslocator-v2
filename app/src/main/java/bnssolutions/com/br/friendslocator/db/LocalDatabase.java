package bnssolutions.com.br.friendslocator.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import bnssolutions.com.br.friendslocator.utils.bitmap.BitmapUtils;

/**
 * Created by leonardo on 26/07/15.
 */
public class LocalDatabase extends SQLiteOpenHelper {
    public static final int             DB_VERSION = 1;
    public static final String          DB_NAME = "find_my_contacts";
    public static final String          TABLE_USER_PROFILE = "user_profile";
    public static final String          COLLUMN_EMAIL = "email";
    public static final String          COLLUMN_AVATAR = "avatar";

    public LocalDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public LocalDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_PROFILE = String.format("CREATE TABLE %s (%s TEXT, %s BLOB)", TABLE_USER_PROFILE, COLLUMN_EMAIL, COLLUMN_AVATAR);
        db.execSQL(CREATE_USER_PROFILE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addUserProfile(String email) {
        SQLiteDatabase  db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLLUMN_EMAIL, email);
        db.insert(TABLE_USER_PROFILE, null, values);
        db.close();
    }

    public void saveAvatarToProfile(String email, Bitmap avatar) {
        SQLiteDatabase  db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLLUMN_AVATAR, BitmapUtils.toByteArray(avatar));
        db.update(TABLE_USER_PROFILE, values, COLLUMN_EMAIL + "=?", new String[]{email});
        db.close();
    }

    public String getUserProfile() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER_PROFILE, new String[]{COLLUMN_EMAIL}, null, null, null, null, null);
        if(cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            String email = cursor.getString(0);
            cursor.close();
            db.close();

            return email;
        }

        return null;
    }

    public Bitmap getAvatar(String email) {
        Bitmap acquiredBitmap = null;
        String[] columns = new String[] { COLLUMN_AVATAR };
        String where = COLLUMN_EMAIL + "=?";
        String[] whereValues = new String[] { email };
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER_PROFILE, columns, where, whereValues, null, null, null);

        if(cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            byte[] byteArray = cursor.getBlob(0);
            if(byteArray != null && byteArray.length > 0)
                acquiredBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }

        cursor.close();

        return acquiredBitmap;
    }
}
