package bnssolutions.com.br.friendslocator.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.adapters.AddedContactsLISTAdapter;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.SendPrivateMessageService;

public class AddedContactsListActivity extends AppCompatActivity implements Handler.Callback, AdapterView.OnItemClickListener {
    private View mainContainer;
    private ListView listView;
    private AddedContactsLISTAdapter adapter;
    private Handler handler;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_added_contacts_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainContainer = findViewById(R.id.main_content);
        listView = (ListView) findViewById(R.id.list_view);

        Collection<ContactMarker> markers = ContactFactory.getInstance().getContacts().values();
        if(markers.size() > 0) {
            populateListView(markers);
        } else {
            // Contatos ainda não foram baixados ou não existem. É preciso registrar um event
            // em ContactFactory para sermos notificados de que houve alguma mudanca em sua
            // lista de usuarios.
            handler = new Handler(this);
            ContactFactory.getInstance().registerOnUpdateListener(handler);
            dialog = new ProgressDialog(AddedContactsListActivity.this);
            dialog.setIndeterminate(true);
            dialog.setTitle(R.string.downloading_contacts);
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_added_contacts_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean handleMessage(Message msg) {
        dialog.dismiss();
        switch(msg.what) {
            case NetworkManager.DONE:
                ContactFactory.getInstance().unregisterOnUpdateListener(handler);
                Collection<ContactMarker> markers = ContactFactory.getInstance().getContacts().values();

                if(markers.size() > 0)
                    populateListView(markers);
                else
                    Snackbar.make(mainContainer, R.string.err_nocontactsadded, Snackbar.LENGTH_LONG).show();
                break;

            case NetworkManager.ERROR:
                break;

        }
        return true;
    }

    private void populateListView(Collection<ContactMarker> markers) {
        List<Contact> contacts = new ArrayList<Contact>();
        for (ContactMarker marker : markers) {
            contacts.add(marker.getContact());
        }

        adapter = new AddedContactsLISTAdapter(this, R.layout.addedcontacts_list_line, contacts);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Contact contact = adapter.getItem(position);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddedContactsListActivity.this);
        alertDialog.setTitle(R.string.sendnotification_title);
        alertDialog.setMessage(R.string.sendnotification_subtitle);
        alertDialog.setIcon(R.drawable.ic_sendmessage2);

        final EditText txtMsgBody = new EditText(AddedContactsListActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        txtMsgBody.setLayoutParams(layoutParams);
        alertDialog.setView(txtMsgBody);

        alertDialog.setPositiveButton(R.string.button_send,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String message = txtMsgBody.getText().toString();
                        if(message.length() > 0 ) {
                            final ProgressDialog sendPMDialog = new ProgressDialog(AddedContactsListActivity.this);
                            sendPMDialog.setIndeterminate(true);
                            sendPMDialog.setTitle(R.string.privatemessage_preparing);
                            sendPMDialog.setMessage(getResources().getString(R.string.please_wait));
                            sendPMDialog.show();
                            Handler sendPMHandler = new Handler() {
                                @Override
                                public void handleMessage(Message message) {
                                    sendPMDialog.dismiss();
                                    switch(message.what) {
                                        case NetworkManager.DONE:
                                            Snackbar.make(mainContainer, R.string.privatemessage_sent, Snackbar.LENGTH_LONG).show();
                                            break;

                                        case NetworkManager.ERROR:
                                            Snackbar.make(mainContainer, R.string.generic_error, Snackbar.LENGTH_LONG).show();
                                            break;
                                    }
                                }
                            };

                            SendPrivateMessageService service = new SendPrivateMessageService(sendPMHandler, message, contact.getIdentity());
                            service.execute();
                            sendPMDialog.show();
                        } else {
                            Snackbar.make(mainContainer, R.string.status_canceled, Snackbar.LENGTH_LONG).show();
                        }
                    }
                });

        alertDialog.setNegativeButton(R.string.button_cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
}
