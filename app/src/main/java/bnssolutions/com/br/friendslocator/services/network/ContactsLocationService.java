package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.commons.lang3.ObjectUtils;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.BaseRecurrentService;

/**
 * Created by leonardo on 26/07/15.
 */
public class ContactsLocationService extends BaseRecurrentService {
    private Handler           handler;

    public ContactsLocationService(Handler handler) {
        this.handler = handler;
        this.FREQ = 2000;
        this.LC_FREQ = 5000;
    }

    @Override
    public void run() {
        Bundle bundle = new Bundle();
        Message msg = new Message();
        try {
            boolean result = false;
            Map<String, List<Contact>> contacts = NetworkManager.apiInstance.getContactsLocation(ApplicationManager.getInstance().getUserEmail());
            if(contacts.containsKey("online") || contacts.containsKey("offline")) {
                result = true;
            }

            ContactFactory.getInstance().update(contacts);

            bundle.putBoolean("result", result);
            msg.what = NetworkManager.DONE;
        } catch(Exception e) {
            e.printStackTrace();
            bundle.putString("error", e.getMessage());
            msg.what = NetworkManager.ERROR;
        } finally {
            msg.setData(bundle);
        }

        handler.sendMessage(msg);
    }
}
