package bnssolutions.com.br.friendslocator.exceptions;

/**
 * Created by leonardo on 11/09/15.
 */
public class OperationFailedException extends Exception {

    public OperationFailedException(String message) {
        super(message);
    }
}