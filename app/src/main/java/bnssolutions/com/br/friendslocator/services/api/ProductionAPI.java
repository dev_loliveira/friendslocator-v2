package bnssolutions.com.br.friendslocator.services.api;

/**
 * Created by leonardo on 05/01/16.
 */
public class ProductionAPI extends BaseBackendAPI {
    private final String    SERVER_PROTOCOL = "http";
    private final String    SERVER_IP = "ec2-54-187-150-47.us-west-2.compute.amazonaws.com";
    private final String    SERVER_PORT = "80";

    public ProductionAPI() {
        setServerProtocol(SERVER_PROTOCOL);
        setServerIP(SERVER_IP);
        setServerPort(SERVER_PORT);
    }
}

