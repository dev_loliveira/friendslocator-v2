package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 31/07/16.
 */
public class ChangePasswordService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private String password;

    public ChangePasswordService(Handler handler, String password) {
        this.handler = handler;
        this.password = password;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean result = NetworkManager.apiInstance.changePassword(password);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}
