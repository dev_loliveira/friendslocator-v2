package bnssolutions.com.br.friendslocator.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.db.LocalDatabase;
import bnssolutions.com.br.friendslocator.services.network.SendLocationService;
import bnssolutions.com.br.friendslocator.utils.mapmarker.BaseMapMarker;

/**
 * Created by leonardo on 31/07/16.
 */
public class DeviceLocationManager implements Handler.Callback{
    public static DeviceLocationManager instance;
    public BaseMapMarker marker;
    private Bitmap icon;
    private Handler sendLocationHandler;

    private DeviceLocationManager() {
        sendLocationHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what) {
                    case NetworkManager.DONE:
                        break;

                    case NetworkManager.ERROR:
                        break;
                }
            }
        };
    }

    public static DeviceLocationManager getInstance() {
        if(instance == null)
            instance = new DeviceLocationManager();
        return instance;
    }

    public void init(Context context) {
        this.marker = new BaseMapMarker();

        LocalDatabase database = new LocalDatabase(context);
        Bitmap icon = database.getAvatar(ApplicationManager.getInstance().getUserEmail());
        if(icon != null) {
            this.icon = icon;
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        double latitude = msg.getData().getDouble("latitude");
        double longitude = msg.getData().getDouble("longitude");
        boolean updateCamera = msg.getData().getBoolean("update_camera");

        marker.setTitle(ApplicationManager.getInstance().getUserEmail());
        marker.setSnippet(String.format("%f %f", latitude, longitude));
        marker.setPosition(new LatLng(latitude, longitude));

        if(icon == null) {
            Bitmap defaultIcon = BitmapFactory.decodeResource(ApplicationManager.getInstance().getContext().getResources(), R.drawable.ic_defaultusermarker);
            marker.setIcon(defaultIcon);
            ApplicationManager.getInstance().setDrawerIcon(defaultIcon);
        } else {
            marker.setIcon(icon);
            ApplicationManager.getInstance().setDrawerIcon(icon);
        }

        marker.update();

        SendLocationService service = new SendLocationService(sendLocationHandler, latitude, longitude);
        service.execute();

        if(updateCamera) {
            CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        }
        return true;
    }
}
