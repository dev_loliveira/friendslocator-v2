package bnssolutions.com.br.friendslocator.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.exceptions.DuplicatedServiceException;
import bnssolutions.com.br.friendslocator.receivers.PrivateMessagesReceiver;
import bnssolutions.com.br.friendslocator.services.DeviceLocation;
import bnssolutions.com.br.friendslocator.services.network.ContactsLocationService;
import bnssolutions.com.br.friendslocator.services.network.FetchPrivateMessagesService;
import bnssolutions.com.br.friendslocator.services.network.InviteNotificationService;
import bnssolutions.com.br.friendslocator.services.network.UpdateBatteryLevelService;
import bnssolutions.com.br.friendslocator.utils.mapmarker.BaseMapMarker;

/**
 * Created by leonardo on 25/07/16.
 */
public class ApplicationManager implements Handler.Callback {
    private boolean                     RESOURCES_LOADED = false;
    private NavigationView              navView;
    private DrawerLayout                drawerLayout;
    private Toolbar                     toolbar;
    private GoogleMap                   gMap;
    private Context                     context;
    private String                      userEmail;
    private Bitmap                      drawerIcon;
    private InviteNotificationService   inviteService;
    private ContactsLocationService     contactsLocationService;
    private UpdateBatteryLevelService   updateBatteryService;
    private FetchPrivateMessagesService privateMessageService;
    private DeviceLocation              deviceLocation;
    private static ApplicationManager   instance;

    private ApplicationManager() {}

    public static ApplicationManager getInstance() {
        if(instance == null)
            instance = new ApplicationManager();
        return instance;
    }

    public void updateCloudStatus(boolean status) {
        if(RESOURCES_LOADED) {
            MenuItem cloudStatus = toolbar.getMenu().findItem(R.id.cloudStatus);
            if (cloudStatus != null) {
                if (status) {
                    cloudStatus.setIcon(R.drawable.ic_cloudon);
                } else {
                    cloudStatus.setIcon(R.drawable.ic_cloudoff);
                }
            }
        }
    }

    public void updateNetworkStatus(boolean status) {
        updateCloudStatus(status);
    }

    public void redrawMarkersOnMap() {
        gMap.clear();
        for(ContactMarker marker : ContactFactory.getInstance().getOnlineContacts().values()) {
            marker.redraw();
        }

        if(deviceLocation != null) {
            LatLng devicePos = deviceLocation.getLastKnownLocation();
            if (devicePos != null) {
                BaseMapMarker marker = DeviceLocationManager.getInstance().marker;
                if (marker != null) {
                    marker.setPosition(devicePos);
                }
            }
        }
    }

    private void startServices() {
        Handler inviteHandler = new Handler(InvitationManager.getInstance());
        Handler contactsLocationHandler = new Handler(ContactsLocationManager.getInstance());
        Handler deviceLocationHandler = new Handler(DeviceLocationManager.getInstance());
        Handler batteryHandler = new Handler(DeviceBatteryManager.getInstance());

        inviteService = new InviteNotificationService(inviteHandler);
        deviceLocation = new DeviceLocation(context, deviceLocationHandler);
        contactsLocationService = new ContactsLocationService(contactsLocationHandler);
        updateBatteryService = new UpdateBatteryLevelService(batteryHandler);

        inviteService.start();
        contactsLocationService.start();
        deviceLocation.init();
        deviceLocation.start();
        updateBatteryService.start();

        if(FetchPrivateMessagesService.INSTANCES == 0) {
            Handler privateMessageHandler = new Handler(new PrivateMessagesReceiver());
            try {
                privateMessageService = new FetchPrivateMessagesService(privateMessageHandler, getUserEmail());
                privateMessageService.start();
            } catch(DuplicatedServiceException dsE) {}
        }
    }

    public void stopServices() {
        if(inviteService != null)
            inviteService.stop();

        if(contactsLocationService != null)
            contactsLocationService.stop();

        if(deviceLocation != null)
            deviceLocation.stop();
    }

    @Override
    public boolean handleMessage(Message msg) {
        // Responsável para tratar as mensagens relacionadas a atualizações na app
        // (normalmente relacionadas com redraw da tela)
        List<Boolean> requiredAssetsLoaded = new ArrayList<Boolean>(){{
            add(gMap != null);
            add(navView != null);
            add(drawerLayout != null);
            add(toolbar != null);
            add(context != null);
            add(userEmail != null);
        }};
        boolean allLoaded = true;

        for(Boolean loaded : requiredAssetsLoaded) {
            if(!loaded) {
                allLoaded = false;
                break;
            }
        }

        RESOURCES_LOADED = allLoaded;
        if(allLoaded) {
            // Todas as variáveis foram carragadas então podemos prosseguir

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            PrivateMessagesReceiver.ENABLE_VIBRATIONS = settings.getBoolean("enable_vibration", true);
            DeviceLocationManager.getInstance().init(context);
            stopServices();
            startServices();
        }
        return true;
    }

    public NavigationView getNavigationView() { return navView; }
    public void setNavigationView(NavigationView navView) { this.navView = navView; }

    public DrawerLayout getDrawerLayout() { return drawerLayout; }
    public void setDrawerLayout(DrawerLayout drawerLayout) { this.drawerLayout = drawerLayout; }

    public Toolbar getToolbar() { return toolbar; }
    public void setToolbar(Toolbar toolbar) { this.toolbar = toolbar; }

    public GoogleMap getGoogleMap() { return gMap; }
    public void setGoogleMap(GoogleMap gMap) {
        gMap.clear();
        this.gMap = gMap;
        redrawMarkersOnMap();
    }

    public Context getContext() { return context; }
    public void setContext(Context context) {
        this.context = context;
        if(RESOURCES_LOADED)
            DeviceLocationManager.getInstance().init(context);
    }

    public String getUserEmail() { return userEmail; }
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        if(RESOURCES_LOADED) {
            // Source: http://stackoverflow.com/questions/33194594/navigationview-get-find-header-layout
            TextView drawerEmail = (TextView) getNavigationView().getHeaderView(0).findViewById(R.id.navDrawerEmail);
            drawerEmail.setText(ApplicationManager.getInstance().getUserEmail());
        }
    }

    public Bitmap getDrawerIcon() {
        return drawerIcon;
    }

    public void setDrawerIcon(Bitmap icon) {
        this.drawerIcon = icon;
        if(RESOURCES_LOADED) {
            ImageView imgView = (ImageView) getNavigationView().getHeaderView(0).findViewById(R.id.navDrawerIcon);
            imgView.setImageBitmap(icon);
        }
    }
}
