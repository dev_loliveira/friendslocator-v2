package bnssolutions.com.br.friendslocator.activities.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.InvitationListActivity;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;

/**
 * Created by leonardo on 25/07/16.
 */
public class CoreNavDrawer {
    public static CoreNavDrawer     instance;
    private ActionBarDrawerToggle   drawerToggle;
    private Activity                source;

    public static CoreNavDrawer getInstance() {
        if(instance == null)
            instance = new CoreNavDrawer();
        return instance;
    }

    public void configureDrawer(Activity source) {
        this.source = source;
        DrawerLayout drawerLayout = ApplicationManager.getInstance().getDrawerLayout();
        Toolbar toolbar = ApplicationManager.getInstance().getToolbar();

        drawerToggle = new ActionBarDrawerToggle(
                source, drawerLayout, toolbar, R.string.navdrawer_open, R.string.navdrawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }
}
