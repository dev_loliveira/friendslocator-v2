package bnssolutions.com.br.friendslocator.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.AddedContactsListActivity;
import bnssolutions.com.br.friendslocator.activities.CoreActivity;
import bnssolutions.com.br.friendslocator.db.LocalDatabase;
import bnssolutions.com.br.friendslocator.exceptions.DuplicatedServiceException;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.FetchPrivateMessagesService;

/**
 * Created by leonardo on 03/08/16.
 */
public class PrivateMessagesReceiver extends BroadcastReceiver implements Handler.Callback {
    public static boolean SIGNAL_RECV = false;
    public static boolean ENABLE_VIBRATIONS = true;


    public PrivateMessagesReceiver() {
    }

    public void startService() {
        try {
            FetchPrivateMessagesService service;
            service = new FetchPrivateMessagesService(new Handler(this), ApplicationManager.getInstance().getUserEmail());
            service.start();
        } catch(DuplicatedServiceException dE) {
            Log.d("exception", dE.getMessage());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        SIGNAL_RECV = true;
        LocalDatabase database = new LocalDatabase(context);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        ENABLE_VIBRATIONS = settings.getBoolean("enable_vibration", true);

        if(database.getUserProfile() != null) {
            // Somente chama o servico caso o e-mail do usuário tenha sido localizado na base
            // local.
            ApplicationManager.getInstance().setUserEmail(database.getUserProfile());
            ApplicationManager.getInstance().setContext(context);

            startService();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case NetworkManager.DONE:
                /*
                 * Eu poderia criar um manager de notificacoes que ira associar o historico das mensagens
                 * recebidas com seus respectivos contatos.
                 */
                String jsonStr = msg.getData().getString("messages_dict");
                Type type = new TypeToken<Map<String, List<String>>>(){}.getType();
                Map<String, List<String>> messagesDict = new Gson().fromJson(jsonStr, type);

                int index = 0;
                for(String key : messagesDict.keySet()) {
                    for(String message : messagesDict.get(key)) {
                        pushNotification(index++, key, message);
                    }
                }
                break;

            case NetworkManager.ERROR:
                Log.d("error", msg.getData().getString("error"));
                break;
        }

        return true;
    }

    private void pushNotification(int index, String userName, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ApplicationManager.getInstance().getContext());
        builder.setSmallIcon(R.drawable.ic_privatemessage);
        builder.setContentTitle(String.format("@%s", userName));
        builder.setContentText(message);
        builder.setAutoCancel(true);

        if(ENABLE_VIBRATIONS)
            builder.setVibrate(new long[]{1000, 500});

        Intent resultIntent = new Intent(ApplicationManager.getInstance().getContext(), CoreActivity.class);
        resultIntent.putExtra("userEmail", ApplicationManager.getInstance().getUserEmail());
        resultIntent.putExtra("next_activity", AddedContactsListActivity.class.getName());
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(ApplicationManager.getInstance().getContext());
        stackBuilder.addParentStack(CoreActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        Notification notification = builder.build();

        NotificationManager manager = (NotificationManager) ApplicationManager.getInstance().getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(index, notification);
    }
}
