package bnssolutions.com.br.friendslocator.services.api;

/**
 * Created by leonardo on 08/08/15.
 */
public class LocalServerAPI extends BaseBackendAPI {
    private final String    SERVER_PROTOCOL = "http";
    private final String    SERVER_IP = "192.168.25.61";
    private final String    SERVER_PORT = "8000";

    public LocalServerAPI() {
        setServerProtocol(SERVER_PROTOCOL);
        setServerIP(SERVER_IP);
        setServerPort(SERVER_PORT);
    }
}
