package bnssolutions.com.br.friendslocator.activities;

/**
 * Created by leonardo on 03/08/16.
 */
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import bnssolutions.com.br.friendslocator.R;

/**
 * This shows how to create a simple activity with streetview
 */
public class StreetViewActivity extends AppCompatActivity implements OnStreetViewPanoramaReadyCallback {
    private SupportStreetViewPanoramaFragment panoramaFragment;
    private StreetViewPanorama panorama;
    private LatLng startPosition;
    private Bundle lastSavedInstance;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streetview);
        this.lastSavedInstance = savedInstanceState;
        initElements();
    }

    private void initElements() {
        double latitude = getIntent().getDoubleExtra("latitude", -1);
        double longitude = getIntent().getDoubleExtra("longitude", -1);
        this.startPosition = new LatLng(latitude, longitude);

        panoramaFragment = (SupportStreetViewPanoramaFragment) getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
        panoramaFragment.getStreetViewPanoramaAsync(this);

    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        if(lastSavedInstance == null) {
            this.panorama = streetViewPanorama;
            this.panorama.setPosition(startPosition);
        }
    }
}
