package bnssolutions.com.br.friendslocator.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.db.LocalDatabase;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.CreateAccountService;

/**
 * Created by leonardo on 28/07/16.
 */
public class CreateAccountActivity extends AppCompatActivity implements Handler.Callback {
    private TextView   userEmail;
    private EditText   firstName;
    private EditText   lastName;
    private EditText   password;
    private View       container;
    private View       progress;
    private Handler    handler;

    private void initElements() {
        handler = new Handler(this);
        container = findViewById(R.id.createaccount_form);
        progress = findViewById(R.id.createaccount_progress);
        userEmail = (TextView) findViewById(R.id.userEmail);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        password = (EditText) findViewById(R.id.password);

        userEmail.setText(getIntent().getStringExtra("userEmail"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);

        initElements();
    }

    public void sendForm(View view) {
        String _userEmail = userEmail.getText().toString();
        String _firstName = firstName.getText().toString();
        String _lastName = lastName.getText().toString();
        String _password = password.getText().toString();

        new CreateAccountService(handler, _userEmail, _firstName, _lastName, _password).execute();
        showProgress(true);
    }

    @Override
    public boolean handleMessage(Message msg) {
        showProgress(false);
        switch(msg.what) {
            case NetworkManager.DONE:
                boolean registered = msg.getData().getBoolean("registered");
                if(registered) {
                    LocalDatabase database = new LocalDatabase(CreateAccountActivity.this);
                    database.addUserProfile(userEmail.getText().toString());

                    Intent coreIntent = new Intent(getApplicationContext(), CoreActivity.class);
                    coreIntent.putExtra("userEmail", userEmail.getText().toString());
                    coreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(coreIntent);

                }
                break;

            case NetworkManager.ERROR:
                break;
        }
        return false;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            container.setVisibility(show ? View.GONE : View.VISIBLE);
            container.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    container.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progress.setVisibility(show ? View.VISIBLE : View.GONE);
            progress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progress.setVisibility(show ? View.VISIBLE : View.GONE);
            container.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
