package bnssolutions.com.br.friendslocator.exceptions;

import org.apache.http.HttpException;

/**
 * Created by leonardo on 24/07/15.
 */
public class Http404Exception extends HttpException {

    public Http404Exception(String message) {
        super(message);
    }
}
