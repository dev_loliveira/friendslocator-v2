package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 06/08/16.
 */
public class SendPrivateMessageService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private String  to_whom;
    private String  message;

    public SendPrivateMessageService(Handler handler, String message, String to_whom) {
        this.handler = handler;
        this.to_whom = to_whom;
        this.message = message;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean result = NetworkManager.apiInstance.sendPrivateMessage(this.message, this.to_whom);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putBoolean("result", false);
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}
