package bnssolutions.com.br.friendslocator.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.adapters.InvitationListAdapter;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactHolder;
import bnssolutions.com.br.friendslocator.managers.InvitationManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.AcceptInviteService;
import bnssolutions.com.br.friendslocator.services.network.RefuseInviteService;

public class InvitationListActivity extends AppCompatActivity implements Handler.Callback {
    private Toolbar    toolBar;
    private Handler    handler;
    private InvitationListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_list);
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        ListView listView = (ListView) findViewById(R.id.invitations_container);
        adapter = new InvitationListAdapter(this, R.layout.invitation_list_line, InvitationManager.invitations);
        listView.setAdapter(adapter);

        handler = new Handler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_invitation_list, menu);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ContactHolder.reset();
    }

    public void acceptSelected(MenuItem item) {
        for(ContactHolder holder : ContactHolder.getChecked()) {
            AcceptInviteService service = new AcceptInviteService(handler, holder.getContact().getIdentity());
            service.execute();
        }
    }

    public void refuseSelected(MenuItem item) {
        for(ContactHolder holder : ContactHolder.getChecked()) {
            RefuseInviteService service = new RefuseInviteService(handler, holder.getContact().getIdentity());
            service.execute();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case NetworkManager.DONE:
                String who_invited = msg.getData().getString("who_invited");
                Contact contact = ContactHolder.getByIdentity(who_invited).getContact();
                adapter.remove(contact);
                ContactHolder.remove(contact);
                break;

            case NetworkManager.ERROR:
                break;
        }

        return true;
    }
}
