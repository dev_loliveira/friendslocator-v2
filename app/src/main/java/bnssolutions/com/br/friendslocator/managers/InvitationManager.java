package bnssolutions.com.br.friendslocator.managers;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.Contact;

/**
 * Created by leonardo on 25/07/16.
 */
public class InvitationManager implements Handler.Callback {
    private static InvitationManager  instance;
    public static List<Contact>       invitations = new ArrayList<Contact>();

    public static InvitationManager getInstance() {
        if(instance == null)
            instance = new InvitationManager();
        return instance;
    }

    public static void updateCount() {
        TextView txtCount = (TextView) ApplicationManager.getInstance().getNavigationView().getMenu().findItem(R.id.menu_invitations).getActionView();
        txtCount.setText(String.valueOf(invitations.size()));
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case NetworkManager.DONE:
                String jsonData = msg.getData().getString("result");
                Type type = new TypeToken<ArrayList<Contact>>(){}.getType();
                invitations = new Gson().fromJson(jsonData, type);
                updateCount();
                ApplicationManager.getInstance().updateCloudStatus(true);
                break;

            case NetworkManager.ERROR:
                ApplicationManager.getInstance().updateCloudStatus(false);
                break;
        }

        return true;
    }
}
