package bnssolutions.com.br.friendslocator.contacts;

import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardo on 27/07/16.
 */
public class ContactHolder {
    public static List<ContactHolder> instances = new ArrayList<ContactHolder>();
    private Contact   contact;
    private CheckBox  checkBox;
    private TextView  txtView;

    public ContactHolder(Contact contact, CheckBox checkBox, TextView txtView) {
        this.contact = contact;
        this.checkBox = checkBox;
        this.txtView = txtView;
        instances.add(this);
    }

    public void setUp() {
        checkBox.setText(String.format("%s %s", contact.getFirstName(), contact.getLastName()));
        txtView.setText(contact.getIdentity());
    }

    public static void reset() {
        instances.clear();
    }

    public static ContactHolder getByIdentity(String identity) {
        for(ContactHolder holder : instances) {
            if(holder.contact.getIdentity().equals(identity))
                return holder;
        }
        return null;
    }

    public boolean isChecked() {
        return checkBox.isChecked();
    }

    public static List<ContactHolder> getChecked() {
        List<ContactHolder> result = new ArrayList<ContactHolder>();
        for(ContactHolder holder : ContactHolder.instances) {
            if(holder.isChecked())
                result.add(holder);
        }
        return result;
    }

    public Contact getContact() {
        return contact;
    }

    public static void remove(Contact contact) {
        for(ContactHolder holder : ContactHolder.instances) {
            if(holder.contact.getIdentity().equals(contact.getIdentity())) {
                ContactHolder.instances.remove(holder);
                break;
            }
        }
    }
}
