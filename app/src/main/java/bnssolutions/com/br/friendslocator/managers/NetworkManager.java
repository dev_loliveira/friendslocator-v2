package bnssolutions.com.br.friendslocator.managers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import bnssolutions.com.br.friendslocator.services.api.BaseBackendAPI;
import bnssolutions.com.br.friendslocator.services.api.LocalServerAPI;
import bnssolutions.com.br.friendslocator.services.api.ProductionAPI;

/**
 * Created by leonardo on 25/07/16.
 */
public class NetworkManager extends BroadcastReceiver {
    public static BaseBackendAPI     apiInstance = new ProductionAPI();
    public static final int          DONE = 0;
    public static final int          ERROR = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean connected = false;
        if(intent.getExtras()!=null) {
            NetworkInfo ni=(NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(ni!=null && ni.getState()==NetworkInfo.State.CONNECTED) {
                connected = true;
            } else if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
                connected = false;
            }
        }

        ApplicationManager.getInstance().updateNetworkStatus(connected);
    }
}
