package bnssolutions.com.br.friendslocator.managers;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Message;

/**
 * Created by leonardo on 02/08/16.
 */
public class DeviceBatteryManager implements Handler.Callback {
    public static final int         UNKNOWN_LEVEL = -1;
    public static final int         LOW = 0;
    public static final int         MEDIUM = 1;
    public static final int         HIGH = 2;

    private static DeviceBatteryManager instance;

    public static DeviceBatteryManager getInstance() {
        if(instance == null)
            instance = new DeviceBatteryManager();
        return instance;
    }

    public float getBatteryLevel() {
        // Source: https://developer.android.com/training/monitoring-device-state/battery-monitoring.html
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = ApplicationManager.getInstance().getContext().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;
        return batteryPct;
    }

    public int getStatusFromLevel(float level) {
        int status;
        if(level <=100 && level >= 75)
            status = HIGH;
        else if(level <= 74 && level >= 35)
            status = MEDIUM;
        else
            status = LOW;

        return status;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case NetworkManager.DONE:
                ApplicationManager.getInstance().updateCloudStatus(true);
                break;

            case NetworkManager.ERROR:
                ApplicationManager.getInstance().updateCloudStatus(false);
                break;
        }

        return true;
    }
}
