package bnssolutions.com.br.friendslocator.services;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

/**
 * Created by leonardo on 29/01/16.
 */
public abstract class BaseRecurrentService implements Runnable {
    protected int            FREQ;
    protected int            LC_FREQ;
    protected boolean        lowerConsuption = false;
    protected boolean        shouldStop = false;

    public void stopLowConsuption() {
        lowerConsuption = false;
    }

    public void switchToLowConsuption() {
        lowerConsuption = true;
    }

    public void start() {
        runAt(0);
    }

    public void stop() {
        shouldStop = true;
    }

    protected void runAt(int time) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                new Task().execute();
            }
        };
        new Handler().postDelayed(r, time);
    }

    protected void scheduleNextCall() {
        final int freq = lowerConsuption == false ? FREQ : LC_FREQ;
        runAt(freq);
    }

    class Task extends AsyncTask<Void, Void, Message> {

        @Override
        protected Message doInBackground(Void... params) {
            run();
            return null;
        }

        @Override
        protected void onPostExecute(Message msg) {
            if(!shouldStop)
                scheduleNextCall();
        }
    }
}
