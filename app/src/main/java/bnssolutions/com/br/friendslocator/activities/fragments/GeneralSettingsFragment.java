package bnssolutions.com.br.friendslocator.activities.fragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceFragment;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.SettingsActivity;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.ChangePasswordService;

/**
 * Created by leonardo on 31/07/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class GeneralSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Handler.Callback {
    private ProgressDialog  dialog;
    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler(this);
        addPreferencesFromResource(R.xml.pref_general);
        setHasOptionsMenu(true);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch(key) {
            case "password":
                dialog = ProgressDialog.show(getActivity(),
                        getActivity().getResources().getString(R.string.dialog_changepassword_title),
                        getActivity().getResources().getString(R.string.dialog_changepassword_description));
                String password = sharedPreferences.getString(key, "");
                ChangePasswordService service = new ChangePasswordService(handler, password);
                service.execute();
                break;
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        dialog.dismiss();
        switch(msg.what) {
            case NetworkManager.DONE:
                break;

            case NetworkManager.ERROR:
                break;
        }
        return false;
    }
}
