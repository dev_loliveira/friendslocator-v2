package bnssolutions.com.br.friendslocator.db;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.utils.meta.PhoneGroup;

/**
 * Created by leonardo on 29/07/16.
 */
public class PhoneContacts {
    private int              MIN_TIMES_CONTACTED = 0;
    private String           LOG_KEY = "UC";
    private Context context;
    private List<Contact> contacts;

    public PhoneContacts(Context context) {
        this.context = context;
    }

    public void collectContacts() {
        ContentResolver conResolver = context.getContentResolver();
        Cursor cur = conResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Map<String, Contact> contactsDict = new HashMap<String, Contact>();
        contacts = new ArrayList<Contact>();

        if(cur != null && cur.getCount() > 0) {

            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String displayName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                int timesContacted = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.TIMES_CONTACTED));

                if(id != null && displayName != null) {
                    String[] displayName_split = displayName.split(" ");

                    String selection = String.format(
                            "%s AND %s",
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED + " >= ?"
                    );
                    String[] selectionArgs = new String[]{
                            id,
                            Integer.toString(MIN_TIMES_CONTACTED)
                    };

                    Cursor emailCur = conResolver.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            selection,
                            selectionArgs,
                            null
                    );

                    List<String> emails = new ArrayList<String>();
                    while (emailCur.moveToNext()) {
                        String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        emails.add(email);
                    }
                    emailCur.close();

                    for(String email : emails) {
                        Contact contact;
                        if(contactsDict.containsKey(displayName)) {
                            contact = contactsDict.get(displayName);
                            contact.addEmail(email);
                        } else {
                            String firstName;
                            String lastName;
                            if(displayName_split.length > 1) {
                                firstName = displayName_split[0];
                                lastName = displayName_split[1];
                            } else {
                                firstName = displayName;
                                lastName = "";
                            }
                            contact = new Contact(email, firstName, lastName);
                            contactsDict.put(displayName, contact);
                        }
                    }
                }
            }

            contacts.addAll(contactsDict.values());
        } else {
            Log.d(LOG_KEY, "Não foram encontrados contatos neste aparelho :[");
        }
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void sortByName() {

        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact lhs, Contact rhs) {
                return lhs.toString().compareTo(rhs.toString());
            }

            @Override
            public boolean equals(Object object) {
                return false;
            }
        });
    }

    public List<PhoneGroup> getGroups() {
        List<PhoneGroup> groups = new ArrayList<PhoneGroup>();
        String[] projection = new String[] {
                ContactsContract.Groups._ID,
                ContactsContract.Groups.TITLE,
                ContactsContract.Groups.SUMMARY_COUNT,
        };
        String where = null;
        String[] whereArgs = null;

        Cursor groupCursor = context.getContentResolver().query(
                ContactsContract.Groups.CONTENT_SUMMARY_URI,
                projection, where, whereArgs, null
        );

        if(groupCursor != null) {
            if(groupCursor.getCount() > 0) {
                while(groupCursor.moveToNext()) {
                    int id = groupCursor.getInt(groupCursor.getColumnIndex(ContactsContract.Groups._ID));
                    String title = groupCursor.getString(groupCursor.getColumnIndex(ContactsContract.Groups.TITLE));
                    int summaryCount = groupCursor.getInt(groupCursor.getColumnIndex(ContactsContract.Groups.SUMMARY_COUNT));

                    groups.add(new PhoneGroup(id, title, summaryCount));
                }
            }
        }

        return groups;
    }

    public List<Contact> getContactsByGroup(int gId) {
        /*
        * Primeiro obtenho todos os contatod do grupo gId e depois adiciono apenas aqueles que possuem
        * um endereço.
        * */
        ContentResolver cResolver = context.getContentResolver();
        String[] projection = {
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.GroupMembership.CONTACT_ID,
                ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Phone.DATA
        };
        String where = String.format(
                "%s=?",
                ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID
        );
        String[] whereArgs = new String[] {
                Integer.toString(gId),
        };
        Cursor cursor = cResolver.query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                where, whereArgs,
                null
        );

        List<Contact> result = new ArrayList<Contact>();
        if(cursor.getCount() > 0) {
            while(cursor.moveToNext()) {
                String cId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.CONTACT_ID));
                String cName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Cursor emailCur = cResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + cId,
                        null, null
                );
                Cursor phoneCur = cResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + cId,
                        null, null
                );
                List<String> emails = new ArrayList<String>();
                List<String> phones = new ArrayList<String>();

                if(emailCur != null) {
                    while (emailCur.moveToNext()) {
                        String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        emails.add(email);
                    }
                }

                if(phoneCur != null) {
                    while (phoneCur.moveToNext()) {
                        String phone = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
                        phones.add(phone);
                    }
                }

                if(emails.size() > 0 || phones.size() > 0) {
                    Contact contact = new Contact();

                    if(cName.split(" ").length > 0) {
                        // O primeiro nome corresponde a String antes do primeiro espaco. Todos os
                        // caracteres apos isso correspondem ao ultimo nome.
                        String firstName = cName.split(" ")[0];
                        String lastName = cName.substring(cName.indexOf(firstName)+firstName.length()).trim();
                        contact.setFirstName(firstName);
                        contact.setLastName(lastName);
                    } else {
                        contact.setFirstName(cName);
                    }

                    if(emails.size() > 0) {
                        for(String email : emails)
                            contact.addEmail(email);
                    }

                    if(phones.size() > 0) {
                        for(String phone : phones)
                            contact.addPhone(phone);
                    }

                    result.add(contact);
                }

            }
        }

        return result;
    }
}
