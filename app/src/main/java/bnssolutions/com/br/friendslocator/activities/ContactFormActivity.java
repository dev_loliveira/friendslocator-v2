package bnssolutions.com.br.friendslocator.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.adapters.ContactsByGroupTABSAdapter;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.ImportContactsService;

/**
 * Created by leonardo on 29/07/16.
 */
public class ContactFormActivity extends AppCompatActivity implements Handler.Callback {
    private View layoutContainer;
    private Contact contact;
    private EditText txtEmail;
    private EditText txtFirstName;
    private EditText txtLastName;
    private Button btnSend;
    private ProgressDialog progress;
    private Handler handler;

    private void initElements() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        layoutContainer = findViewById(R.id.main_content);

        handler = new Handler(this);
        String contactJson = getIntent().getStringExtra("contactJson");
        Type type = new TypeToken<Contact>(){}.getType();
        contact = new Gson().fromJson(contactJson, type);

        txtEmail = (EditText) findViewById(R.id.email);
        txtFirstName = (EditText) findViewById(R.id.first_name);
        txtLastName = (EditText) findViewById(R.id.last_name);
        btnSend = (Button) findViewById(R.id.send_form);
    }

    private void populateForm() {
        if(contact.getEmails().size() > 0)
            txtEmail.setText(contact.getEmails().get(0));
        txtFirstName.setText(contact.getFirstName());
        txtLastName.setText(contact.getLastName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactform);

        initElements();
        populateForm();
    }

    private boolean validateForm() {
        boolean valid = true;
        List<EditText> fields = new ArrayList<EditText>() {{
            add(txtFirstName);
            add(txtLastName);
            add(txtEmail);
        }};

        for(EditText field : fields) {
            if (field.getText().length() == 0) {
                valid = false;
                field.setError("Campo obrigatorio");
            }
        }

        if(!isEmailValid(txtEmail.getText().toString())) {
            valid = false;
            txtEmail.setError("Entre com um e-mail válido.");
        }

        return valid;
    }

    public void sendForm(View view) {
        if(validateForm()) {
            String firstName = txtFirstName.getText().toString();
            String lastName = txtLastName.getText().toString();
            String email = txtEmail.getText().toString();

            contact.setFirstName(firstName);
            contact.setLastName(lastName);
            contact.addEmail(email);

            progress = ProgressDialog.show(ContactFormActivity.this, "Enviando convite", "Por favor aguarde.");
            progress.show();

            List<Contact> contacts = new ArrayList<Contact>() {{
                add(contact);
            }};
            ImportContactsService service = new ImportContactsService(handler, contacts);
            service.execute();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        progress.dismiss();
        String userMsg = "";
        switch(msg.what) {
            case NetworkManager.DONE:
                if(msg.getData().getBoolean("result"))
                    userMsg = "Convite enviado.";
                break;

            case NetworkManager.ERROR:
                userMsg = "Um erro ocorreu. Por favor tente novamente mais tarde.";
                break;
        }

        Snackbar.make(layoutContainer, userMsg, Snackbar.LENGTH_LONG).show();
        return false;
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
