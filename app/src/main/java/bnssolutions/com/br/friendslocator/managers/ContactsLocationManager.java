package bnssolutions.com.br.friendslocator.managers;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.services.network.FetchAvatarService;

import static com.google.android.gms.internal.zzhl.runOnUiThread;

/**
 * Created by leonardo on 26/07/16.
 */
public class ContactsLocationManager implements Handler.Callback {
    private static ContactsLocationManager  instance;

    public static ContactsLocationManager getInstance() {
        if(instance == null)
            instance = new ContactsLocationManager();
        return instance;
    }

    protected void updateContactsOnMap() {
        Runnable r = new Runnable(){
            @Override
            public void run() {
                for(ContactMarker marker : ContactFactory.getInstance().getOnlineContacts().values()) {
                    marker.update();
                }

                for(ContactMarker marker : ContactFactory.getInstance().getOfflineContacts().values()) {
                    marker.remove();
                }
             }
        };

        runOnUiThread(r);
    }

    protected void fetchAvatar() {
        List<String> ids = new ArrayList<String>();
        Handler avatarHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what) {
                    case NetworkManager.DONE:
                        final String identity = msg.getData().getString("identity");
                        final String encImg = msg.getData().getString("avatar");
                        if(encImg != null && encImg.length() > 0) {
                            ContactMarker marker = ContactFactory.getInstance().get(identity);
                            AvatarUpdateManager avatarManager = new AvatarUpdateManager(encImg, marker);
                            avatarManager.start();
                        } else {
                        }
                        break;

                    case NetworkManager.ERROR:
                        break;
                }
            }
        };

        for(ContactMarker cM : ContactFactory.getInstance().getContacts().values()) {
            if(cM.getContact().getAvatar() == null)
                ids.add(cM.getContact().getIdentity());
        }

        for(String id : ids) {
            new FetchAvatarService(avatarHandler, id).execute();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case NetworkManager.DONE:
                if(msg.getData().getBoolean("result")) {
                    // Quando o retorno for TRUE, significa que foram adicionados contatos ao
                    // ContactFactory
                    updateContactsOnMap();
                    fetchAvatar();
                    updateNavDrawerCount();
                }

                ApplicationManager.getInstance().updateCloudStatus(true);
                break;

            case NetworkManager.ERROR:
                ApplicationManager.getInstance().updateCloudStatus(false);
                break;
        }
        return true;
    }

    private void updateNavDrawerCount() {
        TextView txtCount = (TextView) ApplicationManager.getInstance().getNavigationView().getMenu().findItem(R.id.menu_visualizar_contatos).getActionView();
        txtCount.setText(String.valueOf(ContactFactory.getInstance().getContacts().size()));
    }
}
