package bnssolutions.com.br.friendslocator.utils.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by leonardo on 24/07/15.
 */
public class ContentReader {

    public String getContents(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String contents;
        while ((contents = reader.readLine()) != null) {
            out.append(contents);
        }

        in.close();

        return out.toString();
    }
}
