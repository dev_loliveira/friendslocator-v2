package bnssolutions.com.br.friendslocator.exceptions;

/**
 * Created by leonardo on 04/08/16.
 */
public class DuplicatedServiceException extends Exception {
    private static final String ERROR = "Another instance of %s already in memory.";

    public DuplicatedServiceException(String klass) {
        super(String.format(ERROR, klass));
    }
}
