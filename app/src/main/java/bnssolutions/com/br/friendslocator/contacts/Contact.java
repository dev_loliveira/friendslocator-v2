package bnssolutions.com.br.friendslocator.contacts;


import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardo on 19/07/15.
 */
public class Contact {
    private List<String> emails = new ArrayList<String>();
    private List<String> phones = new ArrayList<String>();
    private String       firstName;
    private String       lastName;
    private Bitmap       avatar;
    private LatLng       lastKnownLocation;
    private float        batteryLevel = -1;

    public Contact() { }

    public Contact(String email, String firstName, String lastName) {
        addEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Contact(String email, String firstName, String lastName, Bitmap avatar) {
        addEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
        setAvatar(avatar);
    }

    public void addEmail(String email) {
        emails.add(email);
    }

    public void addPhone(String phone) {
        phones.add(phone);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAvatar(Bitmap avatar) {
        this.avatar = avatar;
    }

    public void setLastKnownLocation(double lat, double lng) {
        this.lastKnownLocation = new LatLng(lat, lng);
    }

    public List<String> getEmails() {
        return emails;
    }

    public List<String> getPhones() {
        return phones;
    }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public Bitmap getAvatar() {
        return avatar;
    }

    public float getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(float batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public LatLng getLastKnownLocation() {
        return lastKnownLocation;
    }

    public String getIdentity() {
        String identity = getFullIdentity();
        if(identity != null && identity.length() > 0) {
            if (identity.contains("|")) {
                String[] split = identity.split("\\|");
                return split[0];
            } else {
                return identity;
            }
        }
        return null;
    }

    public String getFullIdentity() {
        String result = "";
        if(emails.size() > 0) {
            for(String email : emails) {
                if(result.length() > 0)
                    result += "|";
                result += email;
            }
        }

        if(result.length() > 0)
            return result;
        return null;
    }

    public String toString() {
        String result;
        if (firstName != null && lastName != null)
            result = String.format("%s %s", firstName, lastName);
        else if(firstName != null)
            result = firstName;
        else if(lastName != null)
            result = lastName;
        else
            result = getFullIdentity();

        return result;
    }
}
