package bnssolutions.com.br.friendslocator.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactHolder;

/**
 * Created by leonardo on 07/08/15.
 */
public class InvitationListAdapter extends ArrayAdapter<Contact> {

    private List<Contact> items;
    private int layoutResourceId;
    private Context context;

    public InvitationListAdapter(Context context, int layoutResourceId, List<Contact> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ContactHolder holder = null;
        Log.d("getview", String.format("%d",position));

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        Contact contact = (Contact) items.get(position);

        CheckBox check = (CheckBox) row.findViewById(R.id.fullName);
        TextView txtView = (TextView) row.findViewById(R.id.emailAddr);
        holder = new ContactHolder(contact, check, txtView);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Clicar na linha deve desencadear um click no checkbox.
                v.findViewById(R.id.fullName).performClick();
            }
        });

        holder.setUp();

        return row;
    }
}
