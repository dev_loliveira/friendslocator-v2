package bnssolutions.com.br.friendslocator.exceptions;

import org.apache.http.HttpException;

/**
 * Created by leonardo on 24/07/15.
 */
public class ServerException extends HttpException {

    public ServerException(String message) {
        super(message);
    }
}
