package bnssolutions.com.br.friendslocator.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.Contact;

/**
 * Created by leonardo on 29/07/16.
 */
public class ContactsByGroupLISTAdapter extends ArrayAdapter<Contact> {

    private List<Contact> items;
    private int layoutResourceId;
    private Context context;

    public ContactsByGroupLISTAdapter(Context context, int layoutResourceId, List<Contact> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        ContactHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        Contact contact = items.get(position);

        TextView txtFullname = (TextView) row.findViewById(R.id.full_name);
        TextView txtContactInfo = (TextView) row.findViewById(R.id.contact_info);
        holder = new ContactHolder(contact, txtFullname, txtContactInfo);

        holder.setUp();

        return row;
    }

    class ContactHolder {
        private Contact contact;
        private TextView txtFullname;
        private TextView txtContactInfo;

        public ContactHolder(Contact contact, TextView txtFullname, TextView txtContactInfo) {
            this.contact = contact;
            this.txtFullname = txtFullname;
            this.txtContactInfo = txtContactInfo;
        }

        public void setUp() {
            txtFullname.setText(contact.toString());

            if(contact.getEmails().size() > 0)
                txtContactInfo.setText(contact.getEmails().get(0));
            else if(contact.getPhones().size() > 0)
                txtContactInfo.setText(contact.getPhones().get(0));
        }
    }
}