package bnssolutions.com.br.friendslocator.contacts;

import android.os.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.managers.ApplicationManager;

/**
 * Created by leonardo on 26/07/16.
 */
public class ContactFactory {
    private static ContactFactory       instance;
    private Map<String, ContactMarker> contactsOnline;
    private Map<String, ContactMarker> contactsOffline;
    private List<Handler>              onUpdateListeners;

    public ContactFactory() {
        contactsOnline = new HashMap<String, ContactMarker>();
        contactsOffline = new HashMap<String, ContactMarker>();
        onUpdateListeners = new ArrayList<Handler>();
    }

    public static ContactFactory getInstance() {
        if(instance == null)
            instance = new ContactFactory();
        return instance;
    }

    public void update(Map<String, List<Contact>> data) {
        for(String key : data.keySet()) {
            for(Contact c : data.get(key)) {
                String contactKey = c.getEmails().get(0);
                switch(key) {
                    case "online":
                        ContactMarker updateMarker;
                        if(contactsOffline.containsKey(contactKey)) {
                            updateMarker = contactsOffline.get(contactKey);
                            contactsOffline.remove(contactKey);
                            contactsOnline.put(contactKey, updateMarker);
                        } else if(!contactsOnline.containsKey(contactKey)) {
                            // Contato novo baixado
                            updateMarker = new ContactMarker(c);
                            contactsOnline.put(contactKey, updateMarker);
                        } else {
                            updateMarker = contactsOnline.get(contactKey);
                        }

                        updateMarker.getContact().setFirstName(c.getFirstName());
                        updateMarker.getContact().setLastName(c.getLastName());
                        updateMarker.getContact().setLastKnownLocation(
                                c.getLastKnownLocation().latitude,
                                c.getLastKnownLocation().longitude
                        );
                        updateMarker.reconfigureMarker();
                        break;

                    case "offline":
                        if(contactsOnline.containsKey(contactKey)) {
                            ContactMarker marker = contactsOnline.get(contactKey);
                            contactsOffline.put(contactKey, marker);
                            contactsOnline.remove(contactKey);
                        } else if(!contactsOffline.containsKey(contactKey)) {
                            // Contato novo baixado
                            contactsOffline.put(contactKey, new ContactMarker(c));
                        }
                        break;
                }
            }
        }

        dispatchOnUpdateSignal();
    }

    public ContactMarker get(String identity) {
        if(contactsOnline.containsKey(identity))
            return contactsOnline.get(identity);
        else if(contactsOffline.containsKey(identity))
            return contactsOffline.get(identity);

        return null;
    }

    public Map<String, ContactMarker> getOnlineContacts() {
        return contactsOnline;
    }

    public Map<String, ContactMarker> getOfflineContacts() {
        return contactsOffline;
    }

    public  Map<String, ContactMarker> getContacts() {
        Map<String, ContactMarker> all = new HashMap<String, ContactMarker>();

        all.putAll(contactsOnline);
        all.putAll(contactsOffline);

        return all;
    }

    public boolean isOnline(Contact c) {
        return contactsOnline.containsKey(c.getEmails().get(0));
    }

    public void registerOnUpdateListener(Handler handler) {
        // Registro de eventos ao haver alguma atualização na lista de contatos baixados pela rede.
        onUpdateListeners.add(handler);
    }

    public void unregisterOnUpdateListener(Handler handler) {
        onUpdateListeners.remove(handler);
    }

    private void dispatchOnUpdateSignal() {
        for(Handler h : onUpdateListeners) {
            h.sendEmptyMessage(NetworkManager.DONE);
        }
    }

    private void remove(List<Contact> contacts, Map<String, ContactMarker> from) {
        List<String> keys = new ArrayList<String>();
        for(Contact c : contacts)
            keys.add(c.getEmails().get(0));

        for(String key : keys)
            from.remove(key);
    }

    private void removeFromOnline(List<Contact> contacts) {
        remove(contacts, contactsOnline);
    }

    private void removeFromOffline(List<Contact> contacts) {
        remove(contacts, contactsOffline);
    }
}
