package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 26/07/16.
 */
public class FetchAvatarService extends AsyncTask<Void, Void, Message> {
    private Handler  handler;
    private String   userEmail;

    public FetchAvatarService(Handler handler, String userEmail) {
        this.handler = handler;
        this.userEmail = userEmail;
    }

    @Override
    protected Message doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            String data = NetworkManager.apiInstance.getEncodedAvatar(userEmail);
            bundle.putString("identity", userEmail);
            bundle.putString("avatar", data);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
        }

        return message;
    }

    @Override
    protected void onPostExecute(Message message) {
        handler.sendMessage(message);
    }
}
