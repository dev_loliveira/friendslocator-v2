package bnssolutions.com.br.friendslocator.services.api;


import android.graphics.Bitmap;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.exceptions.Http404Exception;
import bnssolutions.com.br.friendslocator.exceptions.OperationFailedException;
import bnssolutions.com.br.friendslocator.exceptions.ServerException;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.utils.bitmap.BitmapUtils;
import bnssolutions.com.br.friendslocator.utils.http.ContentReader;

/**
 * Created by leonardo on 23/07/15.
 */
public class BaseBackendAPI {
    public static boolean MOCK_CALL = false;

    private String        SERVER_PROTOCOL = "http";
    private String        SERVER_IP = "";
    private String        SERVER_PORT = "";
    private ContentReader contentReader = new ContentReader();

    public String getServerProtocol() {
        return SERVER_PROTOCOL;
    }
    public String getServerIP() {
        return SERVER_IP;
    }
    public String getServerPort() {
        return SERVER_PORT;
    }
    public String getServerURI() {
        return mockCalls() == false ? SERVER_PROTOCOL + "://" + SERVER_IP + ":" + SERVER_PORT : "http://0.0.0.0";
    }

    public void setServerProtocol(String protocol) { SERVER_PROTOCOL = protocol; }
    public void setServerIP(String ip) { SERVER_IP = ip; }
    public void setServerPort(String port) { SERVER_PORT = port; }

    public JSONObject getJsonFrom(String contents) throws JSONException {
        return new JSONObject(contents);
    }

    public String getURIForCall(String action_name) throws UnsupportedEncodingException {
        return String.format("%s/api/?action=%s", getServerURI(), action_name);
    }

    public String getURIForCall(String action_name, Map<String,String> params) throws UnsupportedEncodingException {
        Iterator iter = params.entrySet().iterator();
        String strParams = "";
        while(iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            if(strParams.length() == 0)
                strParams = String.format("%s=%s", (String)pair.getKey(), (String) pair.getValue());
            else
                strParams = String.format("%s&%s=%s", strParams, (String)pair.getKey(), (String)pair.getValue());
        }
        return String.format("%s&%s", getURIForCall(action_name), strParams);
    }

    public HttpResponse getResponse(HttpRequestBase method) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(method);
        return response;
    }

    public boolean isRegistered(final String username) throws IOException, Http404Exception, ServerException, JSONException {
        Map<String, String> params = new HashMap<String, String>(){{
            put("id_type", "email");
            put("identity", username);
        }};
        String abs_url = getURIForCall("is_registered", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            return getJsonFrom(contents).getBoolean("result");
        }

        return false;
    }

    public boolean createAccount(String email, String firstName, String lastName, String password) throws IOException, Http404Exception, ServerException, JSONException {
        String abs_url = getURIForCall("register");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_type", "email"));
        params.add(new BasicNameValuePair("identity", email));
        params.add(new BasicNameValuePair("first_name", firstName));
        params.add(new BasicNameValuePair("last_name", lastName));
        params.add(new BasicNameValuePair("password", password));

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            return getJsonFrom(contents).getBoolean("result");
        }

        return false;
    }

    public boolean checkAuthentication(String username, String password) throws IOException, JSONException, Http404Exception, ServerException {
        String abs_url = getURIForCall("check_authentication");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());
        JSONObject json = getJsonFrom(contents);
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200)
            return json.getBoolean("result");

        return false;
    }

    public boolean resetPassword(final String identity) throws IOException, Http404Exception, ServerException, JSONException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("username", identity);
        }};
        String abs_url = getURIForCall("send_remember_password", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            JSONObject json = getJsonFrom(contents);
            return json.getBoolean("result");
        }

        return false;
    }


    public List<Contact> getInvitations(String userEmail) throws Exception {
        String abs_url = String.format("%s/api/get_invitations/%s/", getServerURI(), userEmail);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());

        List<Contact> invitationList = new ArrayList<Contact>();
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            JSONObject json = getJsonFrom(contents);
            Iterator<?> keys = json.keys();
            while(keys.hasNext()) {
                String key = (String) keys.next();
                String firstName = json.getJSONObject(key).getString("first_name");
                String lastName = json.getJSONObject(key).getString("last_name");
                invitationList.add(new Contact(key, firstName, lastName));
            }
        }

        return invitationList;
    }

    public Map<String, List<Contact>> getContactsLocation(final String username) throws IOException, Http404Exception, ServerException, JSONException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("api_user", ApplicationManager.getInstance().getUserEmail());
            put("username", username);
        }};
        String abs_url = getURIForCall("get_contacts_location", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        Map<String, List<Contact>> resultDict = new HashMap<String, List<Contact>>();
        if(statusCode == 200) {
            List<Contact> onlineContacts = new ArrayList<Contact>();
            List<Contact> offlineContacts = new ArrayList<Contact>();
            List<String> keys = new ArrayList<String>(){{
                add("online");
                add("offline");
            }};

            JSONObject rootJson = new JSONObject(contents);
            for(String keyStatus : keys) {
                JSONObject jsonContacts = rootJson.getJSONObject(keyStatus);
                Iterator<?> keyIterOnline = jsonContacts.keys();
                while (keyIterOnline.hasNext()) {
                    String key = (String) keyIterOnline.next();
                    Contact contact = new Contact();

                    JSONObject jsonContact = (JSONObject) jsonContacts.get(key);
                    double latitude = jsonContact.getDouble("latitude");
                    double longitude = jsonContact.getDouble("longitude");
                    String firstName = jsonContact.getString("first_name");
                    String lastName = jsonContact.getString("last_name");
                    float batteryLevel = (float)jsonContact.getDouble("battery_level");

                    contact.addEmail(key);
                    contact.setFirstName(firstName);
                    contact.setLastName(lastName);
                    contact.setLastKnownLocation(latitude, longitude);
                    contact.setBatteryLevel(batteryLevel);

                    if (keyStatus.equals("online"))
                        onlineContacts.add(contact);
                    else
                        offlineContacts.add(contact);
                }
            }

            resultDict.put("online", onlineContacts);
            resultDict.put("offline", offlineContacts);

            return resultDict;
        }

        return resultDict;
    }

    public String getEncodedAvatar(final String username) throws IOException, Http404Exception, ServerException {
        Map<String, String> params = new HashMap<String, String>(){{
            put("api_user", ApplicationManager.getInstance().getUserEmail());
            put("identity", username);
        }};
        String abs_url = getURIForCall("get_encoded_avatar", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            return contents;
        }

        return null;
    }

    public boolean acceptInvite(final String contactEmail) throws IOException, JSONException, Http404Exception, ServerException, NoSuchAlgorithmException, OperationFailedException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("api_user", ApplicationManager.getInstance().getUserEmail());
            put("who_accepts", ApplicationManager.getInstance().getUserEmail());
            put("who_invited", contactEmail);
        }};
        String abs_url = getURIForCall("accept_invitationV2", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            try {
                JSONObject json = getJsonFrom(contents);
                if (!json.isNull("result"))
                    return json.getBoolean("result");
                else
                    throw new OperationFailedException(json.getJSONObject("error").getString("reason"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean refuseInvite(final String contactEmail) throws IOException, JSONException, Http404Exception, ServerException, NoSuchAlgorithmException, OperationFailedException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("api_user", ApplicationManager.getInstance().getUserEmail());
            put("who_refuses", ApplicationManager.getInstance().getUserEmail());
            put("who_invited", contactEmail);
        }};
        String abs_url = getURIForCall("refuse_invitationV2", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            try {
                JSONObject json = getJsonFrom(contents);
                if (!json.isNull("result"))
                    return json.getBoolean("result");
                else
                    throw new OperationFailedException(json.getJSONObject("error").getString("reason"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean importContacts(List<Contact> contactList) throws IOException, Http404Exception, JSONException, OperationFailedException, ServerException {
        String abs_url = getURIForCall("import_contacts");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("api_user", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("id_type", "email"));
        params.add(new BasicNameValuePair("identity", ApplicationManager.getInstance().getUserEmail()));
        for(Contact c : contactList) {
            params.add(new BasicNameValuePair("c_identities", c.getIdentity()));
        }

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());
        JSONObject json = getJsonFrom(contents);
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            if(!json.isNull("result"))
                return json.getBoolean("result");
        }
        return false;
    }

    public boolean sendLocation(Double latitude, Double longitude) throws IOException, Http404Exception, JSONException, ServerException, OperationFailedException {
        String abs_url = getURIForCall("update_location");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("api_user", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("username", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("lati", Double.toString(latitude)));
        params.add(new BasicNameValuePair("long", Double.toString(longitude)));

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            JSONObject json = getJsonFrom(contents);
            if(!json.isNull("result"))
                return json.getBoolean("result");
            else
                throw new OperationFailedException(json.getJSONObject("error").getString("reason"));
        }

        return false;
    }

    public boolean changePassword(final String password) throws IOException, Http404Exception, JSONException, ServerException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("api_user", ApplicationManager.getInstance().getUserEmail());
            put("username", ApplicationManager.getInstance().getUserEmail());
            put("password", password);
        }};
        String abs_url = getURIForCall("change_password", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            JSONObject json = getJsonFrom(contents);
            return json.getBoolean("result");
        }

        return false;
    }

    public boolean uploadAvatar(final Bitmap avatarImage) throws IOException {
        String abs_url = getURIForCall("upload_avatar");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("api_user", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("id_type", "email"));
        params.add(new BasicNameValuePair("identity", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("img", BitmapUtils.encodeToBase64(avatarImage)));

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());

        try {
            JSONObject json = new JSONObject(contents);
            return json.getBoolean("result");
        } catch(JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateBatteryLevel(float levelPercentage) throws IOException, Http404Exception, JSONException, ServerException, OperationFailedException {
        String abs_url = getURIForCall("update_battery_level");

        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("api_user", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("battery_percentage", Float.toString(levelPercentage)));

        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode == 200) {
            JSONObject json = getJsonFrom(contents);
            if(!json.isNull("result"))
                return json.getBoolean("result");
            else
                throw new OperationFailedException(json.getJSONObject("error").getString("reason"));
        }

        return false;
    }

    public boolean sendPrivateMessage(final String message, final String userEmail) throws IOException, Http404Exception, JSONException, ServerException {
        List<NameValuePair>  params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("api_user", ApplicationManager.getInstance().getUserEmail()));
        params.add(new BasicNameValuePair("to_whom", userEmail));
        params.add(new BasicNameValuePair("message", message));

        String abs_url = getURIForCall("send_private_message");
        HttpPost post = new HttpPost(abs_url);
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = getResponse(post);

        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();
        boolean result = false;

        if(statusCode == 200) {
            JSONObject rootJson = new JSONObject(contents);
            result = rootJson.getBoolean("result");
        }

        return result;
    }

    public Map<String, List<String>> getPrivateMessages(final String userEmail) throws IOException, Http404Exception, JSONException, ServerException {
        Map<String, String> params = new HashMap<String, String>() {{
            put("api_user", userEmail);
        }};
        String abs_url = getURIForCall("get_private_messages", params);
        HttpResponse response = getResponse(new HttpGet(abs_url));
        String contents = contentReader.getContents(response.getEntity().getContent());
        int statusCode = response.getStatusLine().getStatusCode();
        Map<String, List<String>> result = new HashMap<String, List<String>>();

        if(statusCode == 200) {
            JSONObject rootJson = new JSONObject(contents);
            Iterator<?> keyUsername = rootJson.keys();
            while (keyUsername.hasNext()) {
                String key = (String) keyUsername.next();
                JSONArray jArray = rootJson.getJSONArray(key);
                List<String> messages = new ArrayList<String>();
                for(int i = 0;i < jArray.length(); i++) {
                    messages.add((String)jArray.get(i));
                }
                result.put(key, messages);
            }
        }

        return result;
    }

    public boolean mockCalls() {
        /*
         * Método criado para evitar que a api seja chamada durante a execução dos testes.
         */
        return MOCK_CALL;
    }
}
