package bnssolutions.com.br.friendslocator.managers;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.utils.bitmap.BitmapUtils;

import static com.google.android.gms.internal.zzhl.runOnUiThread;

/**
 * Created by leonardo on 28/07/16.
 */
public class AvatarUpdateManager extends Thread implements Handler.Callback {
    private Handler    handler;
    private String encodedImage;
    private ContactMarker marker;

    public AvatarUpdateManager(String encodedImage, ContactMarker marker) {
        handler = new Handler(this);
        this.encodedImage = encodedImage;
        this.marker = marker;
    }

    @Override
    public boolean handleMessage(Message msg) {
        if(ContactFactory.getInstance().isOnline(marker.getContact())) {
            Runnable updateIcon = new Runnable() {
                @Override
                public void run() {
                    marker.update();
                }
            };

            runOnUiThread(updateIcon);
        }

        return true;
    }

    @Override
    public void run() {
        Bitmap decodedIcon = BitmapUtils.decodeFromBase64(encodedImage);
        marker.getContact().setAvatar(decodedIcon);
        marker.setIcon(decodedIcon);
        handler.sendEmptyMessage(1);
    }
}
