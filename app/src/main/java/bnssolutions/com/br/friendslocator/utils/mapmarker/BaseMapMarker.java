package bnssolutions.com.br.friendslocator.utils.mapmarker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;


import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.utils.bitmap.BitmapUtils;


/**
 * Created by leonardo on 27/07/15.
 */
public class BaseMapMarker {
    private Marker           currentMarker;
    private MarkerOptions    mOptions = new MarkerOptions();


    public BaseMapMarker() {
    }

    public Marker getMarker() {
        return currentMarker;
    }

    public void setTitle(String title) {
        mOptions.title(title);
    }

    public void setSnippet(String snippet) {
        mOptions.snippet(snippet);
    }

    public void setPosition(LatLng position) {
        mOptions.position(position);
    }

    public void setDraggable(boolean draggable) {
        mOptions.draggable(draggable);
    }

    public void setIcon(Bitmap icon) {
        // Source: http://stackoverflow.com/questions/1540272/android-how-to-overlay-a-bitmap-draw-over-a-bitmap
        // http://stackoverflow.com/questions/11932805/cropping-circular-area-from-bitmap-in-android
        Context context = ApplicationManager.getInstance().getContext();
        Bitmap portrait = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_frame);
        Bitmap rIcon = Bitmap.createScaledBitmap(icon, portrait.getWidth(), portrait.getHeight(), false);
        Bitmap bMaster = Bitmap.createBitmap(rIcon.getWidth(), rIcon.getHeight(), rIcon.getConfig());

        Canvas c = new Canvas(bMaster);
        c.drawBitmap(rIcon, 0, 0, null);
        Bitmap bcMaster = BitmapUtils.transformToRoundedCornerBitmap(bMaster, 0xff424242, 3, 3, context);

        mOptions.icon(BitmapDescriptorFactory.fromBitmap(bcMaster));
    }

    public String getTitle() {
        return mOptions.getTitle();
    }

    public String getSnippet() {
        return mOptions.getSnippet();
    }
    public LatLng getPosition() {
        return mOptions.getPosition();
    }

    public boolean isDraggable() {
        return mOptions.isDraggable();
    }

    private Marker addMarkerToMap() {
        return ApplicationManager.getInstance().getGoogleMap().addMarker(mOptions);
    }

    public void remove() {
        if(currentMarker != null) {
            currentMarker.remove();
            currentMarker = null;
        }
    }

    public void redraw() {
        currentMarker.remove();
        currentMarker = addMarkerToMap();
    }

    public Marker update() {
        if(currentMarker == null) {
            currentMarker = addMarkerToMap();
        }

        currentMarker.setPosition(mOptions.getPosition());
        currentMarker.setTitle(mOptions.getTitle());
        currentMarker.setSnippet(mOptions.getSnippet());

        if(mOptions.getIcon() != null)
            currentMarker.setIcon(mOptions.getIcon());

        return currentMarker;
    }
}
