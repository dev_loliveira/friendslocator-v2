package bnssolutions.com.br.friendslocator.contacts;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.utils.mapmarker.BaseMapMarker;

/**
 * Created by leonardo on 09/08/15.
 */
public class ContactMarker extends BaseMapMarker {
    // Variavel utilizara para informar ao codigo que determinados trechos devem ser ignorados para
    // evitar falhas nos testes em função da não implementacao de classes finais (Ex: Geocoder)
    protected static boolean _MOCK = false;
    private Contact contact;

    public ContactMarker(Contact contact) {
        super();
        this.contact = contact;
        setUp();
    }

    public void setUp() {
        setPosition(contact.getLastKnownLocation());
        setTitle(String.format("%s %s", contact.getFirstName(), contact.getLastName()));
        setSnippet(getAddressInfo(contact.getLastKnownLocation()));

        if(contact.getAvatar() != null)
            setIcon(contact.getAvatar());
    }

    protected String getAddressInfo(LatLng position) {
        if(ContactMarker._MOCK == false) {
            try {
                Geocoder geocoder = new Geocoder(ApplicationManager.getInstance().getContext(), Locale.getDefault());
                List<Address> addrList = geocoder.getFromLocation(position.latitude, position.longitude, 1);
                return addrList.get(0).getAddressLine(0);
            } catch(IOException ioE) {
            } catch(NullPointerException nullE) {
            } catch(IndexOutOfBoundsException indexE) {
            }
        } else {
            return "MOCK";
        }

        return "";
    }

    public Contact getContact() {
        return contact;
    }

    public void reconfigureMarker() {
        setTitle(contact.toString());
        setSnippet(contact.getEmails().get(0));
        setPosition(contact.getLastKnownLocation());
    }

    @Override
    public Marker update() {
        Marker marker = super.update();
        return marker;
    }
}
