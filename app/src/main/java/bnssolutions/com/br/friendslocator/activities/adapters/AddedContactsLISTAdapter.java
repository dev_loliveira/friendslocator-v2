package bnssolutions.com.br.friendslocator.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.utils.bitmap.BitmapUtils;

/**
 * Created by leonardo on 30/07/16.
 */
public class AddedContactsLISTAdapter extends ArrayAdapter<Contact> {
    private List<Contact> items;
    private int layoutResourceId;
    private Context context;

    public AddedContactsLISTAdapter(Context context, int layoutResourceId, List<Contact> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        ContactHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        Contact contact = items.get(position);

        ImageView imgViewAvatar = (ImageView) row.findViewById(R.id.avatar);
        ImageView imgNetworkStatus = (ImageView) row.findViewById(R.id.ic_networkstatus);
        TextView txtNetworkStatusOnline = (TextView) row.findViewById(R.id.txt_networkstatus_online);
        TextView txtNetworkStatusOffline = (TextView) row.findViewById(R.id.txt_networkstatus_offline);

        TextView txtFullname = (TextView) row.findViewById(R.id.full_name);
        TextView txtContactInfo = (TextView) row.findViewById(R.id.contact_info);
        holder = new ContactHolder(contact, imgViewAvatar, imgNetworkStatus, txtNetworkStatusOnline, txtNetworkStatusOffline, txtFullname, txtContactInfo);

        holder.setUp();

        return row;
    }

    class ContactHolder {
        private Contact contact;
        private ImageView imgViewAvatar;
        private ImageView imgNetworkStatus;
        private TextView txtNetworkStatusOnline;
        private TextView txtNetworkStatusOffline;
        private TextView txtFullname;
        private TextView txtContactInfo;

        public ContactHolder(Contact contact, ImageView imgViewAvatar, ImageView imgNetworkStatus, TextView txtNetworkStatusOnline, TextView txtNetworkStatusOffline, TextView txtFullname, TextView txtContactInfo) {
            this.contact = contact;
            this.imgViewAvatar = imgViewAvatar;
            this.imgNetworkStatus = imgNetworkStatus;
            this.txtNetworkStatusOnline = txtNetworkStatusOnline;
            this.txtNetworkStatusOffline = txtNetworkStatusOffline;
            this.txtFullname = txtFullname;
            this.txtContactInfo = txtContactInfo;
        }

        public void setUp() {
            Resources resources = ApplicationManager.getInstance().getContext().getResources();
            txtFullname.setText(contact.toString());

            if(contact.getAvatar() != null) {
                Bitmap defaultIcon = ((BitmapDrawable)imgViewAvatar.getDrawable()).getBitmap();
                imgViewAvatar.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                BitmapUtils.transformToRoundedCornerBitmap(contact.getAvatar(), R.color.blue, 2, 2, context),
                                defaultIcon.getWidth(),
                                defaultIcon.getHeight(),
                                false)
                );
            }

            if(ContactFactory.getInstance().isOnline(contact)) {
                imgNetworkStatus.setImageResource(R.drawable.ic_online);
                txtNetworkStatusOnline.setVisibility(View.VISIBLE);
                txtNetworkStatusOffline.setVisibility(View.GONE);
            }
            else {
                imgNetworkStatus.setImageResource(R.drawable.ic_offline);
                txtNetworkStatusOffline.setVisibility(View.VISIBLE);
                txtNetworkStatusOnline.setVisibility(View.GONE);
            }

            if(contact.getEmails().size() > 0)
                txtContactInfo.setText(contact.getEmails().get(0));
            else if(contact.getPhones().size() > 0)
                txtContactInfo.setText(contact.getPhones().get(0));
        }
    }
}
