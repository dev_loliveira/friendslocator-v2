package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.List;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 29/07/16.
 */
public class ImportContactsService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private List<Contact> contacts;

    public ImportContactsService(Handler handler, List<Contact> contacts) {
        this.handler = handler;
        this.contacts = contacts;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean result = NetworkManager.apiInstance.importContacts(contacts);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putBoolean("result", false);
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}