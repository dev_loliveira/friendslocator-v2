package bnssolutions.com.br.friendslocator.managers;

import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.Map;

import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.utils.mapmarker.BaseMapMarker;

/**
 * Created by leonardo on 02/08/16.
 */
public class MarkerManager {
    private static MarkerManager instance;

    private MarkerManager() { }

    public static MarkerManager getInstance() {
        if(instance == null)
            instance = new MarkerManager();
        return instance;
    }

    public Map<String, BaseMapMarker> getMarkerOrigin(Marker marker) {
        Map<String, BaseMapMarker> result = new HashMap<String, BaseMapMarker>();
        BaseMapMarker deviceMarker = DeviceLocationManager.getInstance().marker;

        if(deviceMarker != null && deviceMarker.getMarker().getId().equals(marker.getId()))
            result.put(DeviceLocationManager.class.getName(), deviceMarker);
        else {
            for (ContactMarker baseMarker : ContactFactory.getInstance().getOnlineContacts().values()) {
                if (baseMarker != null && baseMarker.getMarker().getId().equals(marker.getId()))
                    result.put(ContactFactory.class.getName(), baseMarker);
            }
        }

        return result;
    }
}
