package bnssolutions.com.br.friendslocator.activities.ui;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.adapters.InfoWindowAdapter;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;


public abstract class CoreUI extends AppCompatActivity implements GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {
    protected Toolbar                  toolbar;
    protected DrawerLayout             drawerLayout;
    protected NavigationView           navigationView;

    protected void initElements() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);
    }

    private void setUpMapIfNeeded() {
        GoogleMap map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if(map != null) {
            map.getUiSettings().setMapToolbarEnabled(false);
            map.setMyLocationEnabled(false);
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            map.setInfoWindowAdapter(new InfoWindowAdapter(this));
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);

            ApplicationManager.getInstance().setGoogleMap(map);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.core, menu);
        return true;
    }
}
