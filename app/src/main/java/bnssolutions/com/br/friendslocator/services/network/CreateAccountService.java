package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 28/07/16.
 */
public class CreateAccountService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private String  userEmail;
    private String firstName;
    private String lastName;
    private String password;

    public CreateAccountService(Handler handler, String userEmail, String firstName, String lastName, String password) {
        this.handler = handler;
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.lastName =  lastName;
        this.password = password;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean registered = NetworkManager.apiInstance.createAccount(userEmail, firstName, lastName, password);
            bundle.putString("userEmail", userEmail);
            bundle.putBoolean("registered", registered);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}
