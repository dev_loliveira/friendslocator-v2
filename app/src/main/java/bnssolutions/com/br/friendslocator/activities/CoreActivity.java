package bnssolutions.com.br.friendslocator.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.ui.CoreNavDrawer;
import bnssolutions.com.br.friendslocator.activities.ui.CoreUI;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;

public class CoreActivity extends CoreUI implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    public void initElements() {
        super.initElements();
        ApplicationManager.getInstance().setNavigationView(navigationView);
        ApplicationManager.getInstance().setDrawerLayout(drawerLayout);
        ApplicationManager.getInstance().setToolbar(toolbar);
        ApplicationManager.getInstance().setUserEmail(getIntent().getStringExtra("userEmail"));
        ApplicationManager.getInstance().setContext(getApplicationContext());
        navigationView.setNavigationItemSelectedListener(this);

        CoreNavDrawer.getInstance().configureDrawer(this);

        Handler handler = new Handler(ApplicationManager.getInstance());
        handler.sendEmptyMessage(0);

        if(getIntent().hasExtra("next_activity")) {
            try {
                Class klass = Class.forName(getIntent().getStringExtra("next_activity"));
                Intent intent = new Intent(CoreActivity.this, klass);
                startActivity(intent);
            } catch(ClassNotFoundException cnfE) {}
        }
    }

    private boolean isGPSEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void buildGPSDialogBox() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(CoreActivity.this);
        builder.setMessage(getResources().getString(R.string.gpsdisabled_summary))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.option_yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.option_no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        initElements();

        if(!isGPSEnabled()) {
            buildGPSDialogBox();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ApplicationManager.getInstance().setContext(getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        ApplicationManager.getInstance().setContext(getApplicationContext());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstance) {
        super.onSaveInstanceState(savedInstance);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationManager.getInstance().stopServices();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_conta) {
            Intent contaIntent = new Intent(CoreActivity.this, SettingsActivity.class);
            startActivity(contaIntent);
        } else if(id == R.id.menu_maptype_normal) {
            ApplicationManager.getInstance().getGoogleMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if(id == R.id.menu_maptype_sattelite) {
            ApplicationManager.getInstance().getGoogleMap().setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else if(id == R.id.menu_invitations) {
            Intent invIntent = new Intent(CoreActivity.this, InvitationListActivity.class);
            startActivity(invIntent);
        } else if(id == R.id.menu_adicionar_contatos) {
            Intent addContactsIntent = new Intent(CoreActivity.this, ContactsByGroupActivity.class);
            startActivity(addContactsIntent);
        } else if(id == R.id.menu_visualizar_contatos) {
            Intent viewContactsIntent = new Intent(CoreActivity.this, AddedContactsListActivity.class);
            startActivity(viewContactsIntent);
        } else if(id == R.id.menu_alteraravatar) {
            Intent avatarIntent = new Intent(CoreActivity.this, AvatarPickerActivity.class);
            startActivity(avatarIntent);
        }

        ApplicationManager.getInstance().getDrawerLayout().closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        final View container = findViewById(R.id.maptoolbar_container);
        container.setVisibility(View.VISIBLE);
        View streetView = container.findViewById(R.id.ic_streetview);
        streetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latitude = marker.getPosition().latitude;
                double longitude = marker.getPosition().longitude;

                container.setVisibility(View.INVISIBLE);

                Intent streetViewIntent = new Intent(getApplicationContext(), StreetViewActivity.class);
                streetViewIntent.putExtra("latitude", latitude);
                streetViewIntent.putExtra("longitude", longitude);
                startActivity(streetViewIntent);
            }
        });
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        View container = findViewById(R.id.maptoolbar_container);
        container.setVisibility(View.INVISIBLE);
    }
}
