package bnssolutions.com.br.friendslocator.activities.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.activities.fragments.ContactsListFragment;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.db.PhoneContacts;
import bnssolutions.com.br.friendslocator.utils.meta.PhoneGroup;

/**
 * Created by leonardo on 29/07/16.
 */
public class ContactsByGroupTABSAdapter extends FragmentPagerAdapter {
    private List<PhoneGroup> groups;

    public ContactsByGroupTABSAdapter(FragmentManager fm, Context context) {
        super(fm);
        List<PhoneGroup> tempGroups = new PhoneContacts(context).getGroups();
        // Adiciona às tabs apenas os grupos que possuem contatos definidos.
        groups = new ArrayList<PhoneGroup>();
        for(PhoneGroup g : tempGroups)
            if(g.getContactsCount() > 0)
                groups.add(g);
    }

    @Override
    public Fragment getItem(int position) {
        PhoneGroup group = groups.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt("groupId", group.getId());
        bundle.putString("groupName", group.getName());
        Fragment fragment = new ContactsListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return groups.get(position).getName();
    }
}
