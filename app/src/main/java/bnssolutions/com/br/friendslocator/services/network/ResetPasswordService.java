package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 29/07/16.
 */
public class ResetPasswordService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private String  userEmail;

    public ResetPasswordService(Handler handler, String userEmail) {
        this.handler = handler;
        this.userEmail = userEmail;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean result = NetworkManager.apiInstance.resetPassword(userEmail);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putBoolean("result", false);
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}