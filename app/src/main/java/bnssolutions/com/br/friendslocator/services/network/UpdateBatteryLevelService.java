package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.DeviceBatteryManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.BaseRecurrentService;

/**
 * Created by leonardo on 02/08/16.
 */
public class UpdateBatteryLevelService extends BaseRecurrentService {
    private Handler handler;

    public UpdateBatteryLevelService(Handler handler) {
        this.handler = handler;
        this.FREQ = 1000 * 60 * 10;
        this.LC_FREQ = 1000 * 60 * 15;
    }

    @Override
    public void run() {
        Bundle bundle = new Bundle();
        Message message = new Message();
        float levelPercentage = DeviceBatteryManager.getInstance().getBatteryLevel();
        try {
            boolean result = NetworkManager.apiInstance.updateBatteryLevel(levelPercentage);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            e.printStackTrace();
            bundle.putString("error", e.getMessage());
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }
    }
}
