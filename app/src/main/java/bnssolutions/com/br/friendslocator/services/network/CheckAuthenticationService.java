package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 28/07/16.
 */
public class CheckAuthenticationService extends AsyncTask<Void, Void, Void> {
    private Handler handler;
    private String  userEmail;
    private String  password;

    public CheckAuthenticationService(Handler handler, String userEmail, String password) {
        this.handler = handler;
        this.userEmail = userEmail;
        this.password = password;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean isValid = NetworkManager.apiInstance.checkAuthentication(userEmail, password);
            bundle.putString("userEmail", userEmail);
            bundle.putBoolean("isValid", isValid);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}
