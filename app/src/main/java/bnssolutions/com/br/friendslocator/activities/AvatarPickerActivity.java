package bnssolutions.com.br.friendslocator.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.widget.Toast;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.db.LocalDatabase;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.managers.DeviceLocationManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.UploadAvatarService;

/**
 * Created by leonardo on 11/08/15.
 */
public class AvatarPickerActivity extends Activity implements Handler.Callback {
    private final int             AVATAR_SIZE = 120;
    private final int             PICK_IMAGE_REQUEST = 1;
    private Handler               handler;
    private ProgressDialog        dialog;
    protected Bitmap              resizedChoosenImage;

    private void initElements() {
        handler = new Handler(this);
    }

    private void openFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent chooser = intent.createChooser(intent, "Escolha uma imagem");
        startActivityForResult(chooser, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_avatarpicker);
        initElements();
        openFileChooser();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int PICK_IMAGE_REQUEST = 1;

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                resizedChoosenImage = Bitmap.createScaledBitmap(bitmap, AVATAR_SIZE, AVATAR_SIZE, false);

                UploadAvatarService service = new UploadAvatarService(handler, resizedChoosenImage);
                service.execute();

                dialog = ProgressDialog.show(AvatarPickerActivity.this,
                        getResources().getString(R.string.dialog_uploadavatar_title),
                        getResources().getString(R.string.dialog_uploadavatar_summary));
            } catch (Exception e) {
                Toast.makeText(this, "Ocorreu um erro, por favor tente novamente mais tarde", Toast.LENGTH_LONG).show();
            }
        } else if(resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        dialog.dismiss();
        switch(msg.what) {
            case NetworkManager.DONE:
                LocalDatabase database = new LocalDatabase(getApplicationContext());
                database.saveAvatarToProfile(ApplicationManager.getInstance().getUserEmail(), resizedChoosenImage);
                finish();
                break;

            case NetworkManager.ERROR:
                openFileChooser();
                Toast.makeText(this, msg.getData().getString("error"), Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }
}
