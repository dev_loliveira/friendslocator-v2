package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.commons.lang3.ObjectUtils;

import java.net.UnknownHostException;
import java.util.List;

import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.BaseRecurrentService;

/**
 * Created by leonardo on 31/07/15.
 */
public class InviteNotificationService extends BaseRecurrentService {
    private Handler           handler;

    public InviteNotificationService(Handler handler) {
        this.handler = handler;
        this.FREQ = 2000;
        this.LC_FREQ = 5000;
    }

    @Override
    public void run() {
        Bundle bundle = new Bundle();
        Message msg = new Message();
        try {
            List<Contact> contacts = NetworkManager.apiInstance.getInvitations(ApplicationManager.getInstance().getUserEmail());
            bundle.putString("result", new Gson().toJson(contacts));
            msg.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            msg.what = NetworkManager.ERROR;
        } finally {
            msg.setData(bundle);
        }

        handler.sendMessage(msg);
    }
}
