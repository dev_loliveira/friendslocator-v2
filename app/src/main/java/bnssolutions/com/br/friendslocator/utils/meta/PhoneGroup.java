package bnssolutions.com.br.friendslocator.utils.meta;

/**
 * Created by leonardo on 29/07/16.
 */
public class PhoneGroup {
    private int gId;
    private int gSummaryCount;
    private String gName;

    public PhoneGroup(int gId, String gName, int gSummaryCount) {
        this.gId = gId;
        this.gName = gName;
        this.gSummaryCount = gSummaryCount;
    }

    public int getId() { return gId; }
    public String getName() { return gName; }
    public int getContactsCount() { return gSummaryCount; }
}
