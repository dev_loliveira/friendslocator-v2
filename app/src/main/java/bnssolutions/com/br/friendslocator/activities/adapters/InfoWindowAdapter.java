package bnssolutions.com.br.friendslocator.activities.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.BatteryManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.Map;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.contacts.ContactFactory;
import bnssolutions.com.br.friendslocator.contacts.ContactMarker;
import bnssolutions.com.br.friendslocator.managers.ApplicationManager;
import bnssolutions.com.br.friendslocator.managers.DeviceBatteryManager;
import bnssolutions.com.br.friendslocator.managers.DeviceLocationManager;
import bnssolutions.com.br.friendslocator.managers.MarkerManager;
import bnssolutions.com.br.friendslocator.utils.mapmarker.BaseMapMarker;

/**
 * Created by leonardo on 02/08/16.
 */
public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Activity activity;

    public InfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.infowindow_contact, null);
        ImageView avatar = (ImageView) view.findViewById(R.id.avatar);
        TextView fullName = (TextView) view.findViewById(R.id.full_name);
        TextView email = (TextView) view.findViewById(R.id.email);
        ImageView batteryIcon = (ImageView) view.findViewById(R.id.ic_battery);
        TextView batteryRemaining = (TextView) view.findViewById(R.id.battery_remaining_value);

        Map<String, BaseMapMarker> origin = MarkerManager.getInstance().getMarkerOrigin(marker);
        if(origin.keySet().size() > 0) {
            String className = (String) origin.keySet().toArray()[0];
            BaseMapMarker baseMapMarker = origin.get(className);
            if(baseMapMarker != null) {
                if(className.equals(ContactFactory.getInstance().getClass().getName())) {
                    // Marcacao clicada pertence a algum contato
                    ContactMarker contactMarker = (ContactMarker) baseMapMarker;
                    Contact contact = contactMarker.getContact();
                    fullName.setText(contact.toString());
                    email.setText(contact.getEmails().get(0));

                    Resources resources = ApplicationManager.getInstance().getContext().getResources();
                    if(contact.getBatteryLevel() != -1) {
                        switch(DeviceBatteryManager.getInstance().getStatusFromLevel(contact.getBatteryLevel()*100)) {
                            case DeviceBatteryManager.LOW:
                                batteryRemaining.setText(resources.getString(R.string.battery_low_status));
                                break;

                            case DeviceBatteryManager.MEDIUM:
                                batteryRemaining.setText(resources.getString(R.string.battery_medium_status));
                                break;

                            case DeviceBatteryManager.HIGH:
                                batteryRemaining.setText(resources.getString(R.string.battery_high_status));
                                break;
                        }
                    }
                    else
                        batteryRemaining.setText(resources.getString(R.string.battery_unknown_status));
                } else if(className.equals(DeviceLocationManager.getInstance().getClass().getName())) {
                    // Marcacao clicada pertence ao usuario corrente da app
                    float _batteryRemaining = DeviceBatteryManager.getInstance().getBatteryLevel();
                    fullName.setText("");
                    email.setText(ApplicationManager.getInstance().getUserEmail());
                    batteryRemaining.setText(String.format("%s%%", _batteryRemaining*100));
                }
            }
        }

        return view;
    }
}
