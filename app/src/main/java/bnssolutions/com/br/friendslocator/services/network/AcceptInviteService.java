package bnssolutions.com.br.friendslocator.services.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import bnssolutions.com.br.friendslocator.managers.NetworkManager;

/**
 * Created by leonardo on 27/07/16.
 */
public class AcceptInviteService extends AsyncTask<Void, Void, Void> {
    private Handler   handler;
    private String    who_invited;

    public AcceptInviteService(Handler handler, String who_invited) {
        this.handler = handler;
        this.who_invited = who_invited;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bundle bundle = new Bundle();
        Message message = new Message();
        try {
            boolean result = NetworkManager.apiInstance.acceptInvite(who_invited);
            bundle.putString("who_invited", who_invited);
            bundle.putBoolean("result", result);
            message.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putBoolean("result", false);
            message.what = NetworkManager.ERROR;
        } finally {
            message.setData(bundle);
            handler.sendMessage(message);
        }

        return null;
    }
}
