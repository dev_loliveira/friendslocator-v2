package bnssolutions.com.br.friendslocator.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.db.LocalDatabase;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.network.CheckAuthenticationService;
import bnssolutions.com.br.friendslocator.services.network.ResetPasswordService;

/**
 * Created by leonardo on 28/07/16.
 */
public class CheckAuthenticationActivity extends AppCompatActivity implements Handler.Callback {
    private String userEmail;
    private View mProgressView;
    private View mPasswordFormView;
    private EditText txtPassword;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkauthentication);

        initElements();
    }

    private void initElements() {
        userEmail = getIntent().getStringExtra("userEmail");
        handler = new Handler(this);
        mProgressView = findViewById(R.id.password_progress);
        mPasswordFormView = findViewById(R.id.password_form);
        txtPassword = (EditText) findViewById(R.id.password);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mPasswordFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void sendPassword(View view) {
        String password = txtPassword.getText().toString();
        showProgress(true);
        new CheckAuthenticationService(handler, userEmail, password).execute();
    }

    public void resetPassword(View view) {
        Handler resetHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String userMsg = "";
                showProgress(false);
                switch(msg.what) {
                    case NetworkManager.DONE:
                        userMsg = "Senha foi atualizada. Por favor verifique seu e-mail.";
                        break;

                    case NetworkManager.ERROR:
                        userMsg = "Erro ao alterar senha. Por favor tente novamente mais tarde";
                        break;
                }
                Snackbar bar = Snackbar.make(findViewById(R.id.checkauthentication_layout), userMsg, Snackbar.LENGTH_LONG);
                bar.show();
            }
        };

        ResetPasswordService service = new ResetPasswordService(resetHandler, userEmail);
        service.execute();

        showProgress(true);
    }

    @Override
    public boolean handleMessage(Message msg) {
        showProgress(false);
        switch(msg.what) {
            case NetworkManager.DONE:
                boolean isValid = msg.getData().getBoolean("isValid");

                if(isValid) {
                    LocalDatabase database = new LocalDatabase(CheckAuthenticationActivity.this);
                    database.addUserProfile(userEmail);

                    Intent coreIntent = new Intent(CheckAuthenticationActivity.this, CoreActivity.class);
                    coreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    coreIntent.putExtra("userEmail", userEmail);
                    startActivity(coreIntent);

                } else {
                    txtPassword.setError("Senha inválida");
                    txtPassword.requestFocus();
                }
                Log.d("checkauth", Boolean.toString(msg.getData().getBoolean("isValid")));
                break;

            case NetworkManager.ERROR:
                break;
        }

        return true;
    }
}
