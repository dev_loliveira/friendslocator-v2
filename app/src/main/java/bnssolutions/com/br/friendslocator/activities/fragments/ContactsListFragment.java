package bnssolutions.com.br.friendslocator.activities.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.List;

import bnssolutions.com.br.friendslocator.R;
import bnssolutions.com.br.friendslocator.activities.ContactFormActivity;
import bnssolutions.com.br.friendslocator.activities.adapters.ContactsByGroupLISTAdapter;
import bnssolutions.com.br.friendslocator.contacts.Contact;
import bnssolutions.com.br.friendslocator.db.PhoneContacts;

/**
 * Created by leonardo on 29/07/16.
 */
public class ContactsListFragment extends Fragment implements Runnable, Handler.Callback, AdapterView.OnItemClickListener {
    private String groupName;
    private int    groupId;
    private View rootView;
    private ProgressDialog progress;
    private ContactsByGroupLISTAdapter   adapter;
    private ListView listView;
    private List<Contact> contacts;
    private Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_contacts_by_group, container, false);
        listView = (ListView) rootView.findViewById(R.id.list_view);
        progress = ProgressDialog.show(getActivity(), "Acessando contatos", "Por favor aguarde");
        handler = new Handler(this);
        groupName = getArguments().getString("groupName");
        groupId = getArguments().getInt("groupId");

        new Thread(this).start();
        return rootView;
    }

    @Override
    public void run() {
        // Obtem os contatos desse grupo em uma thread separada
        PhoneContacts pC = new PhoneContacts(getActivity());
        contacts = pC.getContactsByGroup(groupId);
        handler.sendEmptyMessage(0);
    }

    @Override
    public boolean handleMessage(Message msg) {
        // Apos obtidos os contatos, defino o adaptador da lista
        adapter = new ContactsByGroupLISTAdapter(getActivity(), R.layout.contact_list_line, contacts);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        progress.dismiss();
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ContactsByGroupLISTAdapter adapter = (ContactsByGroupLISTAdapter) parent.getAdapter();
        Contact c = adapter.getItem(position);

        String contactJson = new Gson().toJson(c);
        Intent formIntent = new Intent(getActivity(), ContactFormActivity.class);
        formIntent.putExtra("contactJson", contactJson);
        startActivity(formIntent);
    }
}