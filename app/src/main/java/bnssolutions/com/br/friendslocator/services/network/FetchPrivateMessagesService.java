package bnssolutions.com.br.friendslocator.services.network;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import bnssolutions.com.br.friendslocator.exceptions.DuplicatedServiceException;
import bnssolutions.com.br.friendslocator.managers.NetworkManager;
import bnssolutions.com.br.friendslocator.services.BaseRecurrentService;

/**
 * Created by leonardo on 03/08/16.
 */
public class FetchPrivateMessagesService extends BaseRecurrentService {
    public static int INSTANCES = 0;
    private Handler handler;
    private String userEmail;

    public FetchPrivateMessagesService(Handler handler, String userEmail) throws DuplicatedServiceException {
        this.handler = handler;
        this.userEmail = userEmail;
        this.FREQ = 1000 * 60 * 2;
        this.LC_FREQ = 1000 * 60 * 5;
        this.FREQ = 6000;

        if(INSTANCES > 0) {
            throw new DuplicatedServiceException(FetchPrivateMessagesService.class.getName());
        }
        INSTANCES++;
    }

    @Override
    public void run() {
        Bundle bundle = new Bundle();
        Message msg = new Message();
        try {
            Map<String, List<String>> messagesDict = NetworkManager.apiInstance.getPrivateMessages(userEmail);
            bundle.putString("messages_dict", new Gson().toJson(messagesDict));
            msg.what = NetworkManager.DONE;
        } catch(Exception e) {
            bundle.putString("error", e.getMessage());
            msg.what = NetworkManager.ERROR;
        } finally {
            msg.setData(bundle);
        }

        handler.sendMessage(msg);
    }
}
