package bnssolutions.com.br.friendslocator.services;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by leonardo on 19/07/15.
 * Classe responsavel por acessar as APIs do Google para descobrir a localização
 * corrente do dispositivo e atualizar o mapa.
 */
public class DeviceLocation implements LocationListener {

    public static Location currentLocation;
    protected boolean firstTime = true;
    private final int TIME_FREQ = 1 * 1000; // 1 segundos
    private final int DIST_FREQ = 0; // 3 metros
    private final int LC_TIME_FREQ = 5 * 1000; // 5 segundos
    private final int LC_DIST_FREQ = 0; // 6 metros
    private boolean lowerConsuption = false;
    private Handler handler;
    private Context context;
    private LocationManager locationManager;
    private LocationProvider provider;

    public DeviceLocation(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    public void init() {
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.provider = getProvider(locationManager);

        try {
            if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
            } else if(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                onLocationChanged(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
            }
        } catch(SecurityException sE) {
            Log.d("devicelocation_ex", sE.getMessage());
        }
    }

    public LatLng getLastKnownLocation() {
        Location location;
        try {
            if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } else if(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else {
                location = null;
            }

            if(location != null)
                return new LatLng(location.getLatitude(), location.getLongitude());
        } catch(SecurityException sE) {
            Log.d("devicelocation_ex", sE.getMessage());
        }

        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        Bundle bundle = new Bundle();
        Message message = new Message();
        if(firstTime) {
            firstTime = false;
            bundle.putBoolean("update_camera", true);
        } else {
            bundle.putBoolean("update_camera", false);
        }

        message.what = 0;
        bundle.putDouble("latitude", location.getLatitude());
        bundle.putDouble("longitude", location.getLongitude());
        message.setData(bundle);
        handler.sendMessage(message);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch(status) {
            case LocationProvider.OUT_OF_SERVICE:
                break;

            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                break;

            default:
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    protected LocationProvider getProvider(LocationManager locationManager) {
        Criteria serviceCriteria = new Criteria();
        serviceCriteria.setAccuracy(Criteria.ACCURACY_FINE);

        List<String> providers =  locationManager.getProviders(true);
        if(providers.size() > 0) {
            if(providers.contains("gps")) {
                provider = locationManager.getProvider("gps");
            }

            else if(providers.contains("network")) {
                provider = locationManager.getProvider("network");
            }

            else {
                provider = locationManager.getProvider("passive");
            }
        }

        return provider;
    }

    public void stopLowerConsuption() {
        lowerConsuption = false;
        restart();
    }

    public void switchToLowerConsuption() {
        lowerConsuption = true;
        restart();
    }

    public boolean isLowerConsuption() {
        return lowerConsuption;
    }

    public void start() {
        try {
            if (isLowerConsuption())
                locationManager.requestLocationUpdates(provider.getName(), LC_TIME_FREQ, LC_DIST_FREQ, this);
            else {
                Log.d("devicelocation", provider.getName());
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_FREQ, DIST_FREQ, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_FREQ, DIST_FREQ, this);
            }
        } catch(SecurityException sE) {
            Log.d("devicelocation_ex", sE.getMessage());
        }
    }

    public void stop() {
        try {
            locationManager.removeUpdates(this);
        } catch(SecurityException sE) {
            Log.d("devicelocation_ex", sE.getMessage());
        }
    }

    public void restart() {
        this.stop();
        this.start();
    }
}
