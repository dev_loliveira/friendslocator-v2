An android project that uses the GPS to trace the location of the user contacts.


*** Building ****
* Efetuar o download da [Sdk](http://developer.android.com/intl/pt-br/sdk/index.html)
* Para baixar uma versao especifica da Sdk, links abaixo:
    * [platform-tools](http://dl-ssl.google.com/android/repository/platform-tools_rXX-linux.zip)
    * [tools](http://dl-ssl.google.com/android/repository/tools_rXX-linux.zip)


*** Ícones ***
    * Avatares: http://iconka.com
    * [icons](https://icons8.com/web-app/for/androidL/server)
